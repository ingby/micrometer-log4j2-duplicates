package com.ingby.micrometer.duplicates;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {
    private final static Logger logger = LogManager.getLogger("com.ingby.micrometer.duplicates.Application");

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        logger.debug("Started");
    }

}
