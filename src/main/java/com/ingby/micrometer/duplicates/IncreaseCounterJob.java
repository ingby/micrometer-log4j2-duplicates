package com.ingby.micrometer.duplicates;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class IncreaseCounterJob {

    private static final Logger logger = LogManager.getLogger(IncreaseCounterJob.class);

    private final ServiceA serviceA;
    private final ServiceB serviceB;
    private final ServiceC serviceC;
    private final ServiceD serviceD;


    @Autowired
    public IncreaseCounterJob(ServiceA serviceA, ServiceB serviceB, ServiceC serviceC, ServiceD serviceD) {
        this.serviceA = serviceA;
        this.serviceB = serviceB;
        this.serviceC = serviceC;
        this.serviceD = serviceD;
        logger.trace("Initialized");
    }

    @Scheduled(fixedRate = 1000)
    public void simulateEverySecond() {
            serviceA.doLog(String.format("%s logging every second", Level.INFO), Level.INFO, "1 OPS/S");
    }

    @Scheduled(fixedRate = 5000)
    public void simulateEveryFiveSeconds() {
        serviceB.doLog(String.format("%s logging every five seconds", Level.WARN), Level.WARN, "0.2 OPS/S");
    }

    @Scheduled(fixedRate = 10000)
    public void simulateEveryTenSeconds() {
        serviceC.doLog(String.format("%s logging every ten seconds", Level.ERROR), Level.ERROR, "0.1 OPS/S");
    }

    @Scheduled(fixedRate = 2000)
    public void simulateEveryTwoSeconds() {
        serviceD.doLog(String.format("%s logging every two seconds", Level.FATAL), Level.FATAL, "0.5 OPS/S");
    }

}