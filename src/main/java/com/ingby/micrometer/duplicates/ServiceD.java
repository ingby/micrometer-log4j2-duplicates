package com.ingby.micrometer.duplicates;

import io.micrometer.core.instrument.MeterRegistry;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

public class ServiceD {
    private final Logger logger;

    private final MeterRegistry meterRegistry;
    private int counter;

    public ServiceD(MeterRegistry meterRegistry, Logger logger, int logCount) {
        this.meterRegistry = meterRegistry;
        this.logger = logger;
        this.counter = logCount;
    }

    public void doLog(String message, Level level, String fixedrate) {
        if (counter-- > 0) {
            logger.log(level, message);
            meterRegistry.counter("test.counter",
                    "fixedrate", fixedrate,
                    "loglevel", level.name())
                    .increment();
        } else {
            System.out.println("No more logging from ServiceD");
        }
    }
}
