package com.ingby.micrometer.duplicates;

import io.micrometer.core.instrument.MeterRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyServiceConfig {
    private static final Logger loggerA = LogManager.getLogger(ServiceA.class);
    private static final Logger loggerB = LogManager.getLogger(ServiceB.class);
    private static final Logger loggerC = LogManager.getLogger(ServiceC.class);
    private static final Logger loggerD = LogManager.getLogger(ServiceD.class);

    @Bean
    public ServiceA serviceA(@Autowired MeterRegistry meterRegistry) {
        return new ServiceA(meterRegistry, loggerA, 30);
    }

    @Bean
    public ServiceB serviceB(@Autowired MeterRegistry meterRegistry) {
        return new ServiceB(meterRegistry, loggerB, 6);
    }

    @Bean
    public ServiceC serviceC(@Autowired MeterRegistry meterRegistry) {
        return new ServiceC(meterRegistry, loggerC, 3);
    }

    @Bean
    public ServiceD serviceD(@Autowired MeterRegistry meterRegistry) {
        return new ServiceD(meterRegistry, loggerD, 15);
    }

}
