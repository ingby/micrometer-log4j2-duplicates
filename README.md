# README #


## Purpose ##

Demonstrate issue with Micrometer metrics for Log4J2, which causes logging events to be counted multiple times.

https://github.com/micrometer-metrics/micrometer/issues/1862

## How do I get set up? ##

Run the program with maven

```
$ mvn spring-boot:run
```

Let the program at least 30 seconds. The output will be

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.1.RELEASE)

[INFO ] 2020-02-22 09:23:10.050 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:10.053 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[ERROR] 2020-02-22 09:23:10.053 com.ingby.micrometer.duplicates.ServiceC: ERROR logging every ten seconds
[FATAL] 2020-02-22 09:23:10.053 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:11.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:12.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:12.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:13.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:14.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:14.049 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:15.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:15.049 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[INFO ] 2020-02-22 09:23:16.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:16.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:17.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:18.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:18.049 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:19.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:20.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:20.049 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[ERROR] 2020-02-22 09:23:20.049 com.ingby.micrometer.duplicates.ServiceC: ERROR logging every ten seconds
[FATAL] 2020-02-22 09:23:20.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:21.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:22.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:22.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:23.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:24.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:24.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:25.050 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:25.050 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[INFO ] 2020-02-22 09:23:26.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:26.049 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:27.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:28.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:28.049 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:29.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:30.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:30.050 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[ERROR] 2020-02-22 09:23:30.050 com.ingby.micrometer.duplicates.ServiceC: ERROR logging every ten seconds
[FATAL] 2020-02-22 09:23:30.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:31.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:32.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:32.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:33.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:34.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:34.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:35.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[WARN ] 2020-02-22 09:23:35.050 com.ingby.micrometer.duplicates.ServiceB: WARN logging every five seconds
[INFO ] 2020-02-22 09:23:36.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:36.050 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:37.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[INFO ] 2020-02-22 09:23:38.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
[FATAL] 2020-02-22 09:23:38.049 com.ingby.micrometer.duplicates.ServiceD: FATAL logging every two seconds
[INFO ] 2020-02-22 09:23:39.049 com.ingby.micrometer.duplicates.ServiceA: INFO logging every second
No more logging from ServiceA
No more logging from ServiceB
No more logging from ServiceC
No more logging from ServiceD
No more logging from ServiceA
No more logging from ServiceA
No more logging from ServiceD
...
``` 
 
Then execute the following from another terminal to scrape the metrics from the prometheus endpoint

```
$ curl -s http://localhost:8090/actuator/prometheus|grep -e log4j -e test_counter
# HELP log4j2_events_total Number of fatal level log events
# TYPE log4j2_events_total counter
log4j2_events_total{level="warn",} 18.0
log4j2_events_total{level="debug",} 0.0
log4j2_events_total{level="error",} 9.0
log4j2_events_total{level="trace",} 0.0
log4j2_events_total{level="fatal",} 15.0
log4j2_events_total{level="info",} 90.0
# HELP test_counter_total  
# TYPE test_counter_total counter
test_counter_total{fixedrate="0.1 OPS/S",loglevel="ERROR",} 3.0
test_counter_total{fixedrate="0.5 OPS/S",loglevel="FATAL",} 15.0
test_counter_total{fixedrate="0.2 OPS/S",loglevel="WARN",} 6.0
test_counter_total{fixedrate="1 OPS/S",loglevel="INFO",} 30.0

```

The counters `test_counter` shows the expected number of logging events.
The counter `log4j2_events_total` for level `fatal` is correct and matches the corresponding `test_counter_total`.
The counters `log4j2_events_total` for the other levels is three times too many.

## Conclusions ##

The loggers for ServiceA, ServiceB and ServiceC is configured with the `com.ingby.micrometer.duplicates` logger config 
log4j2.yaml.

The logger for ServiceD is configured with the `com.ingby.micrometer.duplicates.ServiceD` logger config in 
log4j2.yaml.

The bug in micrometer causes the counter to be multipled by the number of loggers that share the same logger config.


## Root cause ##

The class `io.micrometer.core.instrument.binder.logging.Log4j2Metrics` method `bindTo` adds the metrics filter to the 
`loggerConfig` multiple times for `logger`s that share the same config.

```java
    @Override
    public void close() {
        if (metricsFilter != null) {
            Configuration configuration = loggerContext.getConfiguration();
            LoggerConfig rootLoggerConfig = configuration.getRootLogger();
            rootLoggerConfig.removeFilter(metricsFilter);
            loggerContext.getLoggers().stream()
                    .filter(logger -> !logger.isAdditive())
                    .forEach(logger -> {
                        LoggerConfig loggerConfig = configuration.getLoggerConfig(logger.getName());
                        if (loggerConfig != rootLoggerConfig) {
                            loggerConfig.removeFilter(metricsFilter);
                        }
                    });
            loggerContext.updateLoggers(configuration);
            metricsFilter.stop();
        }
    }

```

This happens for those loggers that are available at initialization. It appears to happen loggers that are declared in 
class annotated with `org.springframework.context.annotation.Configuration`.

## Solution ##
Ensure that the metrics filter is only added once per LoggerConfig instance.  